let {Directory} = global.SnappFramework;

module.exports = function (self) {
  self.resources = Directory(__dirname, 'resources');
};
