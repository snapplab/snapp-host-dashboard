const {Log, HttpServer, Port} = global.SnappFramework;

module.exports = function (self) {
  let port = Port(9191);
  self.server = HttpServer();
  self.server.listen(port);
  Log(`${self} is up at http://localhost:${port.number}`);
};
