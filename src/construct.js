const {Log, Store, Namespace, Url} = global.SnappFramework;

const {INFO} = Log;

module.exports = function (self) {
  INFO && INFO('Snapp Host Dashboard');
  self.store = Store.Local(Namespace('host-dashboard'));
  self.serve();

  let {Dashboard} = self;
  let dash = Dashboard();

  self.server.get(Url('/'), dash.render$);
};
